# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/adminLoginForm.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_adminLoginForm(object):
    def setupUi(self, adminLoginForm):
        adminLoginForm.setObjectName("adminLoginForm")
        adminLoginForm.resize(336, 222)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(adminLoginForm)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayoutAdminLogin = QtWidgets.QVBoxLayout()
        self.verticalLayoutAdminLogin.setObjectName("verticalLayoutAdminLogin")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayoutAdminLogin.addItem(spacerItem)
        self.horizontalLayoutAdminLogin = QtWidgets.QHBoxLayout()
        self.horizontalLayoutAdminLogin.setObjectName("horizontalLayoutAdminLogin")
        self.verticalLayoutAdminLogin_2 = QtWidgets.QVBoxLayout()
        self.verticalLayoutAdminLogin_2.setContentsMargins(-1, -1, 0, -1)
        self.verticalLayoutAdminLogin_2.setObjectName("verticalLayoutAdminLogin_2")
        self.userNameLabel = QtWidgets.QLabel(adminLoginForm)
        self.userNameLabel.setObjectName("userNameLabel")
        self.verticalLayoutAdminLogin_2.addWidget(self.userNameLabel)
        self.userPasswordLabel = QtWidgets.QLabel(adminLoginForm)
        self.userPasswordLabel.setObjectName("userPasswordLabel")
        self.verticalLayoutAdminLogin_2.addWidget(self.userPasswordLabel)
        self.horizontalLayoutAdminLogin.addLayout(self.verticalLayoutAdminLogin_2)
        self.verticalLayoutAdminLogin_3 = QtWidgets.QVBoxLayout()
        self.verticalLayoutAdminLogin_3.setObjectName("verticalLayoutAdminLogin_3")
        self.userNameEdit = QtWidgets.QLineEdit(adminLoginForm)
        self.userNameEdit.setObjectName("userNameEdit")
        self.verticalLayoutAdminLogin_3.addWidget(self.userNameEdit)
        self.userPasswordEdit = QtWidgets.QLineEdit(adminLoginForm)
        self.userPasswordEdit.setObjectName("userPasswordEdit")
        self.verticalLayoutAdminLogin_3.addWidget(self.userPasswordEdit)
        self.horizontalLayoutAdminLogin.addLayout(self.verticalLayoutAdminLogin_3)
        self.verticalLayoutAdminLogin.addLayout(self.horizontalLayoutAdminLogin)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayoutAdminLogin.addItem(spacerItem1)
        self.horizontalLayoutAdminLogin_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayoutAdminLogin_2.setObjectName("horizontalLayoutAdminLogin_2")
        self.adminLoginBtn = QtWidgets.QPushButton(adminLoginForm)
        self.adminLoginBtn.setObjectName("adminLoginBtn")
        self.horizontalLayoutAdminLogin_2.addWidget(self.adminLoginBtn)
        self.adminCancelBtn = QtWidgets.QPushButton(adminLoginForm)
        self.adminCancelBtn.setObjectName("adminCancelBtn")
        self.horizontalLayoutAdminLogin_2.addWidget(self.adminCancelBtn)
        self.verticalLayoutAdminLogin.addLayout(self.horizontalLayoutAdminLogin_2)
        self.verticalLayout_2.addLayout(self.verticalLayoutAdminLogin)

        self.retranslateUi(adminLoginForm)
        QtCore.QMetaObject.connectSlotsByName(adminLoginForm)

    def retranslateUi(self, adminLoginForm):
        _translate = QtCore.QCoreApplication.translate
        adminLoginForm.setWindowTitle(_translate("adminLoginForm", "Passolig Yetkili Giriş"))
        self.userNameLabel.setText(_translate("adminLoginForm", "Kullanıcı Adı"))
        self.userPasswordLabel.setText(_translate("adminLoginForm", "Şifre"))
        self.adminLoginBtn.setText(_translate("adminLoginForm", "Giriş"))
        self.adminCancelBtn.setText(_translate("adminLoginForm", "Vazgeç"))


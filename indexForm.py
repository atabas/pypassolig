# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'indexForm.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_indexForm(object):
    def setupUi(self, indexForm):
        indexForm.setObjectName("indexForm")
        indexForm.resize(400, 300)
        self.horizontalLayout = QtWidgets.QHBoxLayout(indexForm)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(10, -1, 10, -1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.adminLoginBtn = QtWidgets.QPushButton(indexForm)
        self.adminLoginBtn.setObjectName("adminLoginBtn")
        self.horizontalLayout_2.addWidget(self.adminLoginBtn)
        self.clientLoginBtn = QtWidgets.QPushButton(indexForm)
        self.clientLoginBtn.setObjectName("clientLoginBtn")
        self.horizontalLayout_2.addWidget(self.clientLoginBtn)
        self.horizontalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(indexForm)
        QtCore.QMetaObject.connectSlotsByName(indexForm)

    def retranslateUi(self, indexForm):
        _translate = QtCore.QCoreApplication.translate
        indexForm.setWindowTitle(_translate("indexForm", "Passolig"))
        self.adminLoginBtn.setText(_translate("indexForm", "Yetkili İşlemleri"))
        self.clientLoginBtn.setText(_translate("indexForm", "Kullanıcı İşlemleri"))


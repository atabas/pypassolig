import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from indexForm import Ui_indexForm
from adminLoginForm import Ui_adminLoginForm

app = QtWidgets.QApplication(sys.argv) # uygulama oluşturuyoruz
appWidget = QtWidgets.QWidget() # QWidget nesnesi oluşturuyoruz


class Passolig(object):

    def __init__(self):
        super().__init__()
        self.getIndexForm()
        sys.exit(app.exec_())  # Kapat butonuna bastığımızda uygulamayı kapatiyoruz

    def getIndexForm(self):
        indexFormUi = Ui_indexForm()  # Tasarladığımız nesneyi bir değişkene atıyoruz
        indexFormUi.setupUi(appWidget)  # Daha önce oluşturduğumuz widget nesnesine tasarımımızı kuruyoruz
        indexFormUi.adminLoginBtn.clicked.connect(self.getAdminLoginForm)
        appWidget.show()  # Widget nesnesini gösteriyoruz

    def getAdminLoginForm(self):
        adminLoginFormUi =Ui_adminLoginForm()
        adminLoginFormUi.setupUi(appWidget)
        appWidget.show()  # Widget nesnesini gösteriyoruz


if __name__ == "__main__":
    Passolig()

